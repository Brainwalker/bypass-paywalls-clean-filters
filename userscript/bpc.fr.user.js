// ==UserScript==
// @name            Bypass Paywalls Clean - fr
// @downloadURL     https://gitlab.com/magnolia1234/bypass-paywalls-clean-filters/-/raw/main/userscript/bpc.fr.user.js
// @updateURL       https://gitlab.com/magnolia1234/bypass-paywalls-clean-filters/-/raw/main/userscript/bpc.fr.user.js
// @match           *://*.fr/*
// @match           *://*.bienpublic.com/*
// @match           *://*.journaldunet.com/*
// @match           *://*.la-croix.com/*
// @match           *://*.ledauphine.com/*
// @match           *://*.ledevoir.com/*
// @match           *://*.lejsl.com/*
// @match           *://*.lesinrocks.com/*
// @match           *://*.loeildelaphotographie.com/*
// @match           *://*.marianne.net/*
// @match           *://*.monacomatin.mc/*
// @match           *://*.parismatch.com/*
// @match           *://*.science-et-vie.com/*
// ==/UserScript==

(function() {
  'use strict';

window.setTimeout(function () {

var fr_groupe_ebra_domains = ['bienpublic.com', 'dna.fr', 'estrepublicain.fr', 'lalsace.fr', 'ledauphine.com', 'lejsl.com', 'leprogres.fr', 'republicain-lorrain.fr', 'vosgesmatin.fr'];
var fr_groupe_la_depeche_domains = ['centrepresseaveyron.fr', 'ladepeche.fr', 'lindependant.fr', 'midi-olympique.fr', 'midilibre.fr', 'nrpyrenees.fr', 'petitbleu.fr'];
var fr_groupe_nice_matin_domains = ['monacomatin.mc', 'nicematin.com', 'varmatin.com'];
var domain;

if (matchDomain('alternatives-economiques.fr')) {
  window.setTimeout(function () {
    let paywall = document.querySelector('#temp-paywall');
    removeDOMElement(paywall);
    let data_ae_poool = document.querySelector('div[data-ae-poool]');
    if (data_ae_poool)
      data_ae_poool.removeAttribute('style');
  }, 500); // Delay (in milliseconds)
}

else if (matchDomain('atlantico.fr')) {
  let paywall = document.querySelector('div.markup[class*="Paywall"]');
  if (paywall)
    paywall.setAttribute('class', 'markup');
}

else if (matchDomain('challenges.fr')) {
  if (window.location.pathname.endsWith('.amp')) {
    amp_unhide_access_hide('="paywall.access OR cha.access"', '="NOT (paywall.access OR cha.access)"');
  } else {
    let amorce = document.querySelector('.user-paying-amorce');
    if (amorce)
      amorce.setAttribute('style', 'display:none !important');
    let content = document.querySelectorAll('.user-paying-content');
    for (let elem of content) {
      elem.classList.remove('user-paying-content');
      elem.removeAttribute('hidden');
    }
    let paywall = document.querySelector('.temp-paywall');
    removeDOMElement(paywall);
  }
}

else if (matchDomain('charliehebdo.fr')) {
  window.setTimeout(function () {
    let paywalled_content = document.querySelector('div.ch-paywalled-content');
    if (paywalled_content)
      paywalled_content.removeAttribute('style');
    let poool_widget = document.querySelector('div#poool-widget');
    removeDOMElement(poool_widget);
  }, 500); // Delay (in milliseconds)
}

else if (matchDomain('elle.fr')) {
  if (window.location.hostname.startsWith('amp.')) {
    amp_unhide_access_hide('="poool.access OR cmi_premium.access"');
  } else {
    let hidden_images = document.querySelectorAll('img[src^="data:image/"][data-src]');
    for (let hidden_image of hidden_images)
      hidden_image.setAttribute('src', hidden_image.getAttribute('data-src'));
    let subscription_bar = document.querySelector('.tc-subscription-bar');
    removeDOMElement(subscription_bar);
  }
}

else if ((domain = matchDomain(fr_groupe_ebra_domains)) && window.location.href.match(/\/\d{4}\/\d{2}\/\d{2}\//)) {
  let url = window.location.href;
  let url_new = url.replace(domain + '/', domain + '/amp/');
  if (!url.includes(domain + '/amp/')) {
    let free = document.querySelector('[class^="paywall"]');
    if (!free) {
      window.setTimeout(function () {
        window.location.href = url_new;
      }, 500); // Delay (in milliseconds)
    }
  } else {
    amp_unhide_access_hide('="access"', '="NOT access"', 'amp-ad, amp-embed');
  }
}

else if (domain = matchDomain(fr_groupe_la_depeche_domains)) {
  let url = window.location.href;
  let url_new = url.replace(domain + '/', domain + '/amp/');
  if (url.includes(domain + '/amp/')) {
    amp_unhide_subscr_section('amp-ad, amp-embed');
  } else {
    let paywall = document.querySelector('div.paywall');
    if (paywall) {
      removeDOMElement(paywall);
      window.location.href = url_new;
    }
  }
}

else if (domain = matchDomain(fr_groupe_nice_matin_domains)) {
  let url = window.location.href;
  let url_new = url.replace(domain + '/', domain + '/amp/');
  if (url.includes(domain + '/amp/')) {
    amp_unhide_access_hide('="access"', '="NOT access"', 'amp-ad, amp-embed');
  } else {
    let paywall = document.querySelector('div#article-teaser');
    if (paywall)
      paywall.removeAttribute('id');
  }
  let ads = document.querySelectorAll('div[class^="ad-slot-"]');
  removeDOMElement(...ads);
}

else if (matchDomain('journaldunet.com')) {
  let reg_wall = document.querySelector('.reg_wall');
  removeDOMElement(reg_wall);
  let entry_reg_wall = document.querySelector('.entry_reg_wall');
  if (entry_reg_wall) {
    entry_reg_wall.removeAttribute('style');
  }
}

else if (matchDomain('la-croix.com')) {
  let url = window.location.href;
  if (!url.includes('la-croix.com/amp/')) {
    let hidden_images = document.querySelectorAll('source[srcset]');
    for (let elem of hidden_images)
      elem.removeAttribute('srcset');
  } else {
    let paywall_block = document.querySelector('#paywall_block');
    let amp_ads = document.querySelectorAll('amp-ad, amp-embed');
    removeDOMElement(paywall_block, ...amp_ads);
  }
}

else if (matchDomain('lanouvellerepublique.fr')) {
  window.localStorage.clear();
  let alert_didacticiel = document.querySelector('div.alert-didacticiel');
  let loading = document.querySelectorAll('span.loading');
  removeDOMElement(alert_didacticiel, ...loading);
}

else if (matchDomain('ledevoir.com')) {
  let counter = document.querySelector('.popup-msg');
  removeDOMElement(counter);
}

else if (matchDomain(['lejdd.fr', 'parismatch.com'])) {
  let poool_banner = document.querySelector('#poool-container');
  let forbidden = document.querySelector('.forbidden');
  removeDOMElement(poool_banner, forbidden);
  let bottom_hide = document.querySelector('.cnt[data-poool-mode="hide"]');
  if (bottom_hide) {
    bottom_hide.removeAttribute('data-poool-mode');
    bottom_hide.removeAttribute('style');
  }
}

else if (matchDomain('lequipe.fr')) {
  let paywall = document.querySelectorAll('.Paywall, .Article__paywall');
  if (paywall.length) {
    removeDOMElement(...paywall);
    let scripts = document.querySelectorAll('script:not([src], [type])');
    let json_script;
    for (let script of scripts) {
      if (script.innerText.includes('window.__NUXT__=')) {
        json_script = script;
        break;
      }
    }
    let article = document.querySelector('div.article__body');
    if (article && json_script) {
      if (json_script.innerText.includes('articleObject:')) {
        let json = json_script.textContent.split('articleObject:')[1].split(',articleType')[0];
        let url_nuxt = json_script.textContent.split('comment_count_url:"')[1].split('",')[0].replace(/\\u002F/g, '/');
        if (url_nuxt && !url_nuxt.includes(window.location.pathname))
          window.location.reload(true);
        let par_type = json.split('paragraphs:[')[1].split(',{__type:')[1].split(',')[0];
        if (par_type) {
          article.innerHTML = '';
          let json_split = json.split('__type:' + par_type);
          json_split.shift();
          let article_dom;
          let article_text = '';
          let parser = new DOMParser();
          let par_main_title = '';
          for (let par_main of json_split) {
            if (par_main.includes(',content:')) {
              let pars;
              if (par_main.split(',content:"').filter(x => !x.startsWith('\\')).length > 2) {
                if (par_main.startsWith(',title:')) {
                  par_main_title = par_main.split(',title:')[1].split(',')[0].replace(/(^\"|\"$)/g, '');
                  article_text += '<p><strong>' + par_main_title + '</strong></p>';
                }
                par_type = json.split('content:')[1].split('"},{__type:')[1].split(',')[0];
                pars = par_main.split('__type:' + par_type);
              } else {
                pars = [par_main];
              }
              for (let par of pars) {
                par = par.split('}')[0];
                if (par.includes(',content:')) {
                  let content = par.split(',content:"')[1].split('",')[0];
                  let par_title = '';
                  if (par.includes(',title:'))
                    par_title = par.split(',title:')[1].split(',')[0].replace(/(^\"|\"$)/g, '');
                  if (content) {
                    par = content.replace('class=', '');
                    if (par_title.length > 2 && par_title !== par_main_title)
                      par = '<strong>' + par_title + '</strong><br><br>' + content;
                    par = par.replace(/\\u003C/g, '<').replace(/\\u003E/g, '>').replace(/\\u002F/g, '/').replace(/\\"/g, '"').replace(/(^\"|\"$)/g, '').replace(/\\t/g, '');
                    article_text += '<p>' + par + '</p>';
                  }
                }
              }
            }
            par_main = par_main.split('}]')[0];
            if (par_main.includes(',media:')) {
              let media = par_main.split(',media:')[1].split('}}')[0] + '}';
              if (media.includes('ratio:')) {
                let ratio = media.split('ratio:')[1].split(',')[0].split('}')[0];
                if (!parseInt(ratio))
                  ratio = 1.5;
                let url = media.split('url:"')[1].split('"')[0].replace(/\\u002F/g, '/').replace('{width}', '400').replace('{height}', parseInt(400 / ratio)).replace('{quality}', '75');
                if (url)
                  article_text += '<p><img src="' + url + '"</img></p>';
              }
            }
          }
          article_dom = parser.parseFromString('<div style="margin:20px; font-famile:"DINNextLTPro-Regular",sans-serif; fot-size:18px;">' + article_text + '</div>', 'text/html');
          article.appendChild(article_dom.querySelector('div'));
        }
      }
    }
  }
}

else if (matchDomain('lesechos.fr') && window.location.href.match(/\d{6,}/)) {
  if (matchDomain('investir.lesechos.fr')) {
    if (!window.location.href.includes('/amp/')) {
      let paywall = document.querySelector('div.bloc-paywall');
      let amphtml = document.querySelector('link[rel="amphtml"]');
      if (paywall && amphtml) {
        removeDOMElement(paywall);
        window.location.href = amphtml.href;
      }
    } else {
      let amp_ads = document.querySelectorAll('amp-ad');
      removeDOMElement(...amp_ads);
    }
  } else {
    window.setTimeout(function () {
      let abo_banner = document.querySelector('div[class*="pgxf3b-2"]');
      let ad_blocks = document.querySelectorAll('[class*="jzxvkd"');
      for (let ad_block of ad_blocks)
        ad_block.setAttribute('style', 'display:none');
      if (abo_banner) {
        removeDOMElement(abo_banner);
        let url = window.location.href;
        let html = document.documentElement.outerHTML;
        let state;
        let split1 = html.split('window.__CONFIG__=')[1];
        let split2 = split1.split('</script>')[0].trim();
        if (split2.includes('; window.__DATA__=')) {
          state = split2.split('; window.__DATA__=')[1].split('; window.__')[0].trim();
        } else
          state = split2.substr(0, split2.length - 1);
        try {
          let data = JSON.parse(state);
          let data_article = data.article ? data.article : data.pageProps;
          let article = data_article.data.stripes[0].mainContent[0].data.description;
          let url_loaded = data_article.data.path;
          if (url_loaded && !url.replace(/%20/g, '').includes(url_loaded))
            window.location.reload(true);
          let paywallNode = document.querySelector('.post-paywall');
          if (paywallNode) {
            let contentNode = document.createElement('div');
            let parser = new DOMParser();
            let article_html = parser.parseFromString('<div>' + article + '</div>', 'text/html');
            let article_par = article_html.querySelector('div');
            if (article_par) {
              contentNode.appendChild(article_par);
              contentNode.className = paywallNode.className;
              paywallNode.parentNode.insertBefore(contentNode, paywallNode);
              removeDOMElement(paywallNode);
              let paywallLastChildNode = document.querySelector('.post-paywall  > :last-child');
              if (paywallLastChildNode) {
                paywallLastChildNode.setAttribute('style', 'height: auto !important; overflow: hidden !important; max-height: none !important;');
              }
            }
          }
          let styleElem = document.head.appendChild(document.createElement('style'));
          styleElem.innerHTML = ".post-paywall::after {height: auto !important;}";
        } catch (err) {
          window.location.reload(true);
        }
      }
    }, 500); // Delay (in milliseconds)
  }
}

else if (matchDomain('lesinrocks.com')) {
  if (window.location.search.startsWith('?amp')) {
    let size_defined = document.querySelector('amp-script.i-amphtml-layout-size-defined');
    if (size_defined)
      size_defined.style = 'overflow:visible !important;';
    let overlays = document.querySelectorAll('section.learn_more, div.sidebar, div.menu-footer, div.tooltip_bib, footer.content-info');
    removeDOMElement(...overlays);
  }
}

else if (matchDomain('lexpress.fr')) {
  let ads = document.querySelectorAll('div.block_pub, div.bottom-bar-full');
  removeDOMElement(...ads);
}

else if (matchDomain('loeildelaphotographie.com')) {
  let paywall = document.querySelector('.paywall');
  if (paywall) {
    paywall.removeAttribute('class');
  }
  let premium_pic_boxes = document.querySelectorAll('.premium-pic-box');
  let banners = document.querySelectorAll('.membership-promo-container, .login_form_litle');
  removeDOMElement(...premium_pic_boxes, ...banners);
  let blurred_images = document.querySelectorAll('img[style*="blur"]');
  for (let blurred_image of blurred_images)
    blurred_image.removeAttribute('style');
}

else if (matchDomain('lopinion.fr')) {
  if (window.location.search.startsWith('?_amp=true'))
    amp_unhide_access_hide('="access"', '="NOT access"');
}

else if (matchDomain('marianne.net')) {
  let paywall = document.querySelector('div.paywall');
  if (paywall) {
    let article_source = document.querySelector('div.article-body[data-content-src]');
    if (article_source) {
      let article_text = decode_utf8(atob(article_source.getAttribute('data-content-src')));
      let parser = new DOMParser();
      let html = parser.parseFromString('<div>' + article_text + '</div>', 'text/html');
      let article = html.querySelector('div');
      article_source.innerHTML = '';
      article_source.appendChild(article);
      article_source.removeAttribute('data-content-src');
    }
    removeDOMElement(paywall);
  }
}

else if (matchDomain('science-et-vie.com')) {
  if (window.location.hostname.startsWith('amp.')) {
    let pars = document.querySelectorAll('.qiota_reserve > p, .qiota_reserve > h2');
    let pars_text = [];
    for (let par of pars) {
      if (pars_text.includes(par.innerText))
        removeDOMElement(par);
      else
        pars_text.push(par.innerText);
    }
    let sizer = document.querySelector('div.article-content > amp-script > i-amphtml-sizer');
    removeDOMElement(sizer);
    let replaced_content = document.querySelector('div.i-amphtml-replaced-content');
    if (replaced_content)
      replaced_content.removeAttribute('class');
  }
}

else if (matchDomain(['sudouest.fr', 'charentelibre.fr', 'larepubliquedespyrenees.fr'])) {
  let url = window.location.href;
  let paywall = document.querySelector('.article-premium-footer');
  if (paywall) {
    let premium = document.querySelector('meta[name="gsoi:premium-content"]');
    if (premium) {
      if (premium.content) {
        let url_premium = window.location.origin + premium.content;
        replaceDomElementExt(url_premium, false, true, 'div.full-content');
      }
    }
    removeDOMElement(paywall);
  }
  window.setTimeout(function () {
    let footer_premium = document.querySelector('.footer-premium');
    removeDOMElement(footer_premium);
  }, 500); // Delay (in milliseconds)
}

}, 1000);

// General Functions

function matchDomain(domains, hostname) {
  var matched_domain = false;
  if (!hostname)
    hostname = window.location.hostname;
  if (typeof domains === 'string')
    domains = [domains];
  domains.some(domain => (hostname === domain || hostname.endsWith('.' + domain)) && (matched_domain = domain));
  return matched_domain;
}

function setCookie(name, value, domain, path, days) {
  window.localStorage.clear();
  var max_age = days * 24 * 60 * 60;
  document.cookie = name + "=" + (value || "") + "; domain=" + domain + "; path=" + path + "; max-age=" + max_age;
}

function removeDOMElement(...elements) {
  for (let element of elements) {
    if (element)
      element.remove();
  }
}

function replaceDomElementExt(url, proxy, base64, selector, text_fail = '', selector_source = selector) {
  let proxyurl = proxy ? 'https://bpc2-cors-anywhere.herokuapp.com/' : '';
  fetch(proxyurl + url, {headers: {"Content-Type": "text/plain", "X-Requested-With": "XMLHttpRequest"} })
  .then(response => {
    let article = document.querySelector(selector);
    if (response.ok) {
      response.text().then(html => {
        if (base64) {
          html = decode_utf8(atob(html));
          selector_source = 'body';
        }
        let parser = new DOMParser();
        let doc = parser.parseFromString(html, 'text/html');
        let article_new = doc.querySelector(selector_source);
        if (article_new) {
          if (article && article.parentNode)
            article.parentNode.replaceChild(article_new, article);
        }
      });
    } else {
      if (!text_fail) {
        if (url.includes('webcache.googleusercontent.com'))
          text_fail = 'BPC > failed to load from Google webcache: '
      }
      if (text_fail && article) {
        let text_fail_div = document.createElement('div');
        text_fail_div.setAttribute('style', 'margin: 0px 50px; font-weight: bold; color: red;');
        text_fail_div.appendChild(document.createTextNode(text_fail));
        if (proxy) {
          let a_link = document.createElement('a');
          a_link.innerText = url;
          a_link.href = url;
          a_link.target = '_blank';
          text_fail_div.appendChild(a_link);
        }
        article.insertBefore(text_fail_div, article.firstChild);
      }
    }
  });
}

function encode_utf8(str) {
  return unescape(encodeURIComponent(str));
}

function decode_utf8(str) {
  return decodeURIComponent(escape(str));
}

function amp_iframes_replace(weblink = false, source = '') {
  let amp_iframes = document.querySelectorAll('amp-iframe' + (source ? '[src*="'+ source + '"]' : ''));
  let elem;
  for (let amp_iframe of amp_iframes) {
    if (!weblink) {
      elem = document.createElement('iframe');
      Object.assign(elem, {
        src: amp_iframe.getAttribute('src'),
        sandbox: amp_iframe.getAttribute('sandbox'),
        height: amp_iframe.getAttribute('height'),
        width: 'auto',
        style: 'border: 0px;'
      });
      amp_iframe.parentElement.insertBefore(elem, amp_iframe);
      removeDOMElement(amp_iframe);
    } else {
      let video_link = document.querySelector('a#bpc_video_link');
      if (!video_link) {
        amp_iframe.removeAttribute('class');
        elem = document.createElement('a');
        elem.id = 'bpc_video_link';
        elem.innerText = 'Video-link';
        elem.setAttribute('href', amp_iframe.getAttribute('src'));
        elem.setAttribute('target', '_blank');
        amp_iframe.parentElement.insertBefore(elem, amp_iframe);
      }
    }
  }
}

function amp_unhide_subscr_section(amp_ads_sel = 'amp-ad, .ad', replace_iframes = true, amp_iframe_link = false, source = '') {
  let preview = document.querySelector('[subscriptions-section="content-not-granted"]');
  removeDOMElement(preview);
  let subscr_section = document.querySelectorAll('[subscriptions-section="content"]');
  for (let elem of subscr_section)
    elem.removeAttribute('subscriptions-section');
  let amp_ads = document.querySelectorAll(amp_ads_sel);
  removeDOMElement(...amp_ads);
  if (replace_iframes)
    amp_iframes_replace(amp_iframe_link, source);
}

function amp_unhide_access_hide(amp_access = '', amp_access_not = '', amp_ads_sel = 'amp-ad, .ad', replace_iframes = true, amp_iframe_link = false, source = '') {
  let access_hide = document.querySelectorAll('[amp-access' + amp_access + '][amp-access-hide]');
  for (let elem of access_hide)
    elem.removeAttribute('amp-access-hide');
  if (amp_access_not) {
    let amp_access_not_dom = document.querySelectorAll('[amp-access' + amp_access_not + ']');
    removeDOMElement(...amp_access_not_dom);
  }
  let amp_ads = document.querySelectorAll(amp_ads_sel);
  removeDOMElement(...amp_ads);
  if (replace_iframes)
    amp_iframes_replace(amp_iframe_link, source);
}

})();
